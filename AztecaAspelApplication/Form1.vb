﻿Imports FirebirdSql.Data.FirebirdClient

Public Class Azteca2Aspel

    Private _strRoute As String  

    Private Function validar()

        Dim _strUser As String = My.Settings.STR_DEF_USER
        Dim _strPass As String = My.Settings.STR_DEF_PASSWORD
        Dim _strSchema As String = My.Settings.STR_DEF_SCHEMA
        Dim _strHost As String = My.Settings.STR_DEF_HOST
        Dim _strEmpresa As String = My.Settings.STR_DEF_EMPRESA

        Return Not (_strSchema = "" Or _strUser = "" Or _strPass = "" Or _strHost = "" Or _strEmpresa = "")

    End Function

    Private Function validarRuta()

        _strRoute = tbArchivo.Text.Trim()

        Return Not _strRoute = ""

    End Function

    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click

        If validar() = False Then

            PreferencesDialog.Show()

        ElseIf validarRuta() = False Then

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else

            ProgressDialog.Show()

            Try

                Dim serviceConverter As ServiceConverter = New ServiceConverter()
                serviceConverter.Route = _strRoute
                Dim _conectionManager As ConnectionManager = New ConnectionManager()
                _conectionManager.User = My.Settings.STR_DEF_USER
                _conectionManager.Pass = My.Settings.STR_DEF_PASSWORD
                _conectionManager.Schema = My.Settings.STR_DEF_SCHEMA
                _conectionManager.Host = My.Settings.STR_DEF_HOST

                Dim _clienteDAO As ClienteDAO = New ClienteDAO()
                _clienteDAO.ConnectionManager = _conectionManager
                _clienteDAO.Empresa = My.Settings.STR_DEF_EMPRESA
                serviceConverter.ClienteDAO = _clienteDAO

                If ComboBox1.SelectedIndex = 1 Then

                    Try

                        serviceConverter.processSaldoCartera()

                    Catch t As FbException

                        If t.ErrorCode = 335544665 Then

                            MessageBox.Show("La información de clientes está duplicada.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        Else

                            Throw t

                        End If

                    End Try


                Else
                    serviceConverter.processCobranza()

                End If
            Catch t As FbException

                If t.ErrorCode = 335544569 Then

                    MessageBox.Show("Verifique que los objetos para la empresa existen en el esquema.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Else

                    Throw t

                End If


            Catch ex As Exception

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End Try

            ProgressDialog.Hide()

        End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Dim fileDlg As New OpenFileDialog()
        fileDlg.Reset()
        fileDlg.DefaultExt = ".xlsx"
        fileDlg.Filter = "Archivos Excel | *.xlsx"
        fileDlg.Multiselect = False


        If (fileDlg.ShowDialog() = DialogResult.OK) Then

            tbArchivo.Text = fileDlg.FileName
            ' Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If

    End Sub

    Private Sub SalirToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem1.Click
        Me.Close()
    End Sub

    Private Sub ConfiguraciónToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguraciónToolStripMenuItem1.Click
        PreferencesDialog.Show()
    End Sub

    Private Sub Azteca2Aspel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ComboBox1.SelectedIndex = 0
    End Sub

    Private Sub AcercaDeToolStripMenuItem1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem1.Click
        AboutBox1.Show()
    End Sub

End Class
