﻿Imports System.Windows.Forms
Imports System.IO
Imports System.Resources

Public Class PreferencesDialog

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub loadCustConnectionSettings()

        tbSchema.Text = My.Settings.STR_DEF_SCHEMA
        tbUser.Text = My.Settings.STR_DEF_USER
        tbPass.Text = My.Settings.STR_DEF_PASSWORD
        tbHost.Text = My.Settings.STR_DEF_HOST
        tbEmpresa.Text = My.Settings.STR_DEF_EMPRESA
    End Sub


    Private Function validar()

        Dim _strUser As String = tbUser.Text.Trim()
        Dim _strPass As String = tbPass.Text.Trim()
        Dim _strSchema As String = tbSchema.Text.Trim()
        Dim _strHost As String = tbHost.Text.Trim()
        Dim _strEmpresa As String = tbEmpresa.Text.Trim()

        Return Not (_strSchema = "" Or _strUser = "" Or _strPass = "" Or _strHost = "" Or _strEmpresa = "")

    End Function


    Private Sub PreferencesDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        loadCustConnectionSettings()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If validar() = False Then

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else

            Try

                Dim empresa As Integer = Integer.Parse(tbEmpresa.Text.Trim())

                My.Settings.STR_DEF_HOST = tbHost.Text.Trim()
                My.Settings.STR_DEF_PASSWORD = tbPass.Text.Trim()
                My.Settings.STR_DEF_USER = tbUser.Text.Trim()
                My.Settings.STR_DEF_SCHEMA = tbSchema.Text.Trim()

                If empresa < 10 Then
                    My.Settings.STR_DEF_EMPRESA = "0" & empresa
                Else
                    My.Settings.STR_DEF_EMPRESA = empresa
                End If

                My.Settings.Save()

                Me.Close()


            Catch ex As Exception
                MessageBox.Show("Asegurese de haber ingresado un valor numérico para la empresa.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If

    End Sub

End Class
