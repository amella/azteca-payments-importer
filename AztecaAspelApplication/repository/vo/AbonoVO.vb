﻿Public Class AbonoVO
    Public _CVE_CLIE As String
    Public _REFER As String
    Public _ID_MOV As Integer
    Public _NUM_CPTO As Integer
    Public _NUM_CARGO As Integer
    Public _CVE_OBS As Integer
    Public _NO_FACTURA As String
    Public _DOCTO As String
    Public _IMPORTE As Double
    Public _FECHA_APLI As Date
    Public _FECHA_VENC As Date
    Public _AFEC_COI As String
    Public _STRCVEVEND As String
    Public _NUM_MONED As Integer
    Public _TCAMBIO As Double
    Public _IMPMON_EXT As Double
    Public _FECHAELAB As Date
    Public _CTLPOL As Integer
    Public _CVE_FOLIO As String
    Public _TIPO_MOV As String
    Public _CVE_BITA As Integer
    Public _SIGNO As Integer
    Public _CVE_AUT As Integer
    Public _USUARIO As Integer
    Public _OPERACIONPL As String
    Public _REF_SIST As String
    Public _NO_PARTIDA As Integer
    Public _MUST_INSERT As Boolean
End Class
