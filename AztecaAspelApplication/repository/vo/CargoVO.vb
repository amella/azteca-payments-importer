﻿Public Class CargoVO
    Public _CVE_CLIE As String
    Public _REFER As String
    Public _NUM_CPTO As Integer
    Public _NUM_CARGO As Integer
    Public _CVE_OBS As Integer
    Public _NO_FACTURA As String
    Public _DOCTO As String
    Public _IMPORTE As Double
    Public _FECHA_APLI As Date
    Public _FECHA_VENC As Date
    Public _AFEC_COI As String
    Public _STRCVEVEND As String
    Public _NUM_MONED As Integer
    Public _TCAMBIO As Double
    Public _IMPMON_EXT As Double
    Public _FECHAELAB As Date
    Public _CTLPOL As Integer
    Public _CVE_FOLIO As String
    Public _TIPO_MOV As String
    Public _CVE_BITA As Integer
    Public _SIGNO As Integer
    Public _CVE_AUT As Integer
    Public _USUARIO As Integer
    Public _ENTREGADA As String
    Public _FECHA_ENTREGA As Date
    Public _STATUS As String
    Public _REF_SIST As String

    Public Sub New(ByRef _CLAVE As String, ByRef _LIMCRED As Double, ByRef _FECHA_VENC As Date)

        Me._REFER = _CLAVE
        Me._NO_FACTURA = _CLAVE
        Me._DOCTO = _CLAVE
        Me._CVE_CLIE = _CLAVE
        Me._IMPORTE = _LIMCRED
        Me._IMPMON_EXT = _LIMCRED
        Me._FECHA_VENC = _FECHA_VENC

    End Sub

End Class
