﻿Public Class ClienteVO

    Public _CLAVE As String
    Public _STATUS As String
    Public _NOMBRE As String
    Public _RFC As String
    Public _CALLE As String
    Public _NUMINT As String
    Public _NUMEXT As String
    Public _CRUZAMIENTOS As String
    Public _CRUZAMIENTOS2 As String
    Public _COLONIA As String
    Public _CODIGO As String
    Public _LOCALIDAD As String
    Public _MUNICIPIO As String
    Public _ESTADO As String
    Public _PAIS As String
    Public _NACIONALIDAD As String
    Public _REFERDIR As String
    Public _TELEFONO As String
    Public _CLASIFIC As String
    Public _FAX As String
    Public _PAG_WEB As String
    Public _CURP As String
    Public _CVE_ZONA As String
    Public _IMPRIR As String
    Public _MAIL As String
    Public _NIVELSEC As Integer
    Public _ENVIOSILEN As String
    Public _EMAILPRED As String
    Public _DIAREV As String
    Public _DIAPAGO As String
    Public _CON_CREDITO As String
    Public _DIASCRED As Integer
    Public _LIMCRED As Double
    Public _SALDO As Double
    Public _LISTA_PREC As Integer
    Public _CVE_BITA As Integer
    Public _ULT_PAGOD As String
    Public _ULT_PAGOM As Double
    Public _ULT_PAGOF As Date
    Public _DESCUENTO As Double
    Public _ULT_VENTAD As String
    Public _ULT_COMPM As Double
    Public _FCH_ULTCOM As Date
    Public _VENTAS As Double
    Public _CVE_VEND As String
    Public _CVE_OBS As Integer
    Public _TIPO_EMPRESA As String
    Public _MATRIZ As String
    Public _PROSPECTO As String
    Public _CALLE_ENVIO As String
    Public _NUMINT_ENVIO As String
    Public _NUMEXT_ENVIO As String
    Public _CRUZAMIENTOS_ENVIO As String
    Public _CRUZAMIENTOS_ENVIO2 As String
    Public _COLONIA_ENVIO As String
    Public _LOCALIDAD_ENVIO As String
    Public _MUNICIPIO_ENVIO As String
    Public _ESTADO_ENVIO As String
    Public _PAIS_ENVIO As String
    Public _CODIGO_ENVIO As String
    Public _CVE_ZONA_ENVIO As String
    Public _REFERENCIA_ENVIO As String
    Public _CUENTA_CONTABLE As String
    Public _ADDENDAF As String
    Public _ADDENDAD As String
    Public _NAMESPACE As String
    Public _METODODEPAGO As String
    Public _NUMCTAPAGO As String
    Public _CARGO As CargoVO
    Public _MUST_INSERT As Boolean
    Public _MUST_INSERT_CLAVE As Boolean
    Public _ABONOS As List(Of AbonoVO)
    Public _TOTAL_PAGADO

    Public Sub New()

        _ABONOS = New List(Of AbonoVO)

    End Sub

End Class
