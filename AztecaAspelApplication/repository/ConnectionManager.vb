﻿Imports FirebirdSql.Data.FirebirdClient

Public Class ConnectionManager

    Private _host As String
    Private _schema As String
    Private _user As String
    Private _pass As String
    
    Public Property Host()
        Get
            Return _host
        End Get
        Set(ByVal value)
            _host = value
        End Set
    End Property

    Public Property Schema()
        Get
            Return _schema
        End Get
        Set(ByVal value)
            _schema = Replace(value, "\", "\\")
        End Set
    End Property

    Public Property User()
        Get
            Return _user
        End Get
        Set(ByVal value)
            _user = value
        End Set
    End Property

    Public Property Pass()
        Get
            Return _pass
        End Get
        Set(ByVal value)
            _pass = value
        End Set
    End Property

    Public Function getFbConnection()
        Dim connectionString As String = "User ID=" & _user & ";Password=" & _pass & ";" & _
               "Database=" & _host & ":" & _schema & "; " & "DataSource=" & _host & ";Charset=NONE;"

        Return New FbConnection(connectionString)

    End Function



End Class