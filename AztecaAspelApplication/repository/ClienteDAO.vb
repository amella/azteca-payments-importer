﻿Imports FirebirdSql.Data.FirebirdClient
Imports System.Threading

Public Class ClienteDAO

    Private _conectionManager As ConnectionManager

    Private _empresa As String

    Public Property Empresa()
        Get
            Return _empresa
        End Get
        Set(ByVal value)
            _empresa = value
        End Set
    End Property

    Public Property ConnectionManager()
        Get
            Return _conectionManager
        End Get
        Set(ByVal value)
            _conectionManager = value
        End Set
    End Property

    Private Function getInsertAbonoString(ByRef abono As AbonoVO)

        Dim fecha As String = "'" & abono._FECHA_VENC.Year & "-" & abono._FECHA_VENC.Month & "-" & abono._FECHA_VENC.Day & "'"
        Dim fechaApp As String = "'" & abono._FECHA_APLI.Year & "-" & abono._FECHA_APLI.Month & "-" & abono._FECHA_APLI.Day & "'"

        Dim foo As String = "INSERT INTO CUEN_DET" & _empresa & "(STRCVEVEND, NUM_CARGO,NUM_CPTO,AFEC_COI,NUM_MONED,TCAMBIO,TIPO_MOV,SIGNO,USUARIO,CVE_CLIE, REFER, NO_FACTURA, DOCTO, IMPORTE, IMPMON_EXT, FECHA_APLI, FECHA_VENC, FECHAELAB, ID_MOV,NO_PARTIDA) " _
        & " SELECT '    1',1,10,'N',1,1,'A',-1,0,'" & abono._CVE_CLIE & "','" & abono._REFER & "','" & abono._NO_FACTURA & "','" & abono._DOCTO & "'," & abono._IMPORTE & "," & abono._IMPMON_EXT & "," & fechaApp & "," & fecha & ",(select dateadd(SECOND,30,current_timestamp) from rdb$database),1, coalesce(max(NO_PARTIDA),0)+1 FROM CUEN_DET01 where CVE_CLIE = '" & abono._CVE_CLIE & "'"

        Return foo

    End Function

    Private Function getUpdateClientForAbono(ByRef abono As AbonoVO)

        Dim fechaApp As String = "'" & abono._FECHA_APLI.Year & "-" & abono._FECHA_APLI.Month & "-" & abono._FECHA_APLI.Day & "'"

        Dim foo = "UPDATE CLIE" & _empresa & " set saldo = saldo -" & abono._IMPORTE & ", ult_pagod='" & abono._DOCTO & "', ult_pagof=" & fechaApp & ", ult_pagom=" & abono._IMPORTE & _
        " where clave='" & abono._CVE_CLIE & "'"

        Return foo

    End Function

    Private Function getSelectAbonoString(ByRef abono As AbonoVO)

        Dim fecha As String = "'" & abono._FECHA_VENC.Year & "-" & abono._FECHA_VENC.Month & "-" & abono._FECHA_VENC.Day & "'"
        Dim fechaApp As String = "'" & abono._FECHA_APLI.Year & "-" & abono._FECHA_APLI.Month & "-" & abono._FECHA_APLI.Day & "'"

        Dim foo As String = "SELECT COUNT(*) from CUEN_DET" & _empresa & " WHERE ID_MOV=1 AND NUM_CARGO=1 AND NUM_CPTO=10 AND AFEC_COI='N' AND NUM_MONED=1 AND TCAMBIO=1 AND TIPO_MOV='A' AND SIGNO=-1 AND USUARIO=0 AND " _
        & "CVE_CLIE='" & abono._CVE_CLIE & "' AND " & "REFER='" & abono._REFER & "' AND " & "NO_FACTURA='" & abono._NO_FACTURA & "' AND " & "DOCTO='" & abono._DOCTO _
        & "' AND " & "IMPORTE=" & abono._IMPORTE & " AND " & "IMPMON_EXT=" & abono._IMPMON_EXT & " AND " & "FECHA_APLI=" & fechaApp & " AND " _
        & "FECHA_VENC=" & fecha & " AND STRCVEVEND='    1'"

        Return foo

    End Function

    Private Function getSelectClientString(ByRef cliente As ClienteVO)

        Dim foo As String = "SELECT COUNT(*) from CLIE" & _empresa & " WHERE CLAVE='" & cliente._CLAVE & "'"

        Return foo

    End Function

    Private Function getClaveExistenteString(ByRef cliente As ClienteVO)

        Dim foo As String = "SELECT COUNT(*) from CLIE_CLIB" & _empresa & " WHERE CVE_CLIE='" & cliente._CLAVE & "'"

        Return foo

    End Function

    Private Function getInsertCargoString(ByRef cargo As CargoVO)

        Dim fecha As String = "'" & cargo._FECHA_VENC.Year & "-" & cargo._FECHA_VENC.Month & "-" & cargo._FECHA_VENC.Day & "'"

        Dim foo As String = "INSERT INTO CUEN_M" & _empresa & _
         "(CVE_OBS,ENTREGADA,NUM_CARGO,NUM_CPTO,NUM_MONED,SIGNO,STRCVEVEND," & _
         "TCAMBIO,TIPO_MOV,USUARIO,REFER, NO_FACTURA, DOCTO, CVE_CLIE,IMPORTE," & _
         "IMPMON_EXT,FECHAELAB, FECHA_VENC, FECHA_APLI) VALUES(0,'N',1,1,1,1," & _
         "'    1',1,'C',0," & _
         "'" & cargo._REFER & "'," & "'" & cargo._NO_FACTURA & "'," & "'" & _
         cargo._DOCTO & "'," & "'" & cargo._CVE_CLIE & "'," & cargo._IMPORTE & _
         "," & cargo._IMPMON_EXT & ",(select current_timestamp from rdb$database)," & fecha & "," & fecha & ")"

        Return foo

    End Function

    Private Function getInsertClientClave(ByRef client As ClienteVO)
        Dim foo = "INSERT INTO CLIE_CLIB" & _empresa & " (CVE_CLIE) values (" & _
        "'" & client._CLAVE & "')"
        Return foo

    End Function

    Private Function getInsertClientString(ByRef client As ClienteVO)

        Dim importe As Double = client._CARGO._IMPORTE
        Dim docto As String = client._CARGO._DOCTO
        Dim fecha As String = "'" & client._CARGO._FECHA_VENC.Year & "-" & client._CARGO._FECHA_VENC.Month & "-" & client._CARGO._FECHA_VENC.Day & "'"
        Dim foo As String = "INSERT INTO CLIE" & _empresa & _
          "(CON_CREDITO,CRUZAMIENTOS,CRUZAMIENTOS2,CVE_OBS,DIASCRED,ENVIOSILEN," & _
          "IMPRIR,MAIL,MATRIZ,NACIONALIDAD,NIVELSEC,PAIS,PROSPECTO,REFERDIR," & _
          "STATUS,TIPO_EMPRESA,ULT_VENTAD,ULT_COMPM,VENTAS,CLAVE,FCH_ULTCOM,NOMBRE, CALLE," & _
          "CODIGO, LOCALIDAD, COLONIA, ESTADO,  LIMCRED, ULT_PAGOD,ULT_PAGOF,ULT_PAGOM,SALDO) VALUES('S','',''," & _
          "0,0,'N','S','S','         2','Mexicana',0,'México','N','','A','M'," & _
          "'" & docto & "'," & importe & ",0," & "'" & client._CLAVE & "'," & fecha & ","

        If client._NOMBRE = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._NOMBRE & "',"
        End If

        If client._CALLE = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._CALLE & "',"
        End If

        If client._CODIGO = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._CODIGO & "',"
        End If

        If client._MUNICIPIO = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._MUNICIPIO & "',"
        End If

        If client._COLONIA = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._COLONIA & "',"
        End If
        If client._ESTADO = "" Then
            foo = foo & "NULL,"
        Else
            foo = foo & "'" & client._ESTADO & "',"
        End If

        foo = foo & client._LIMCRED & ","

        If client._TOTAL_PAGADO <> 0 Then

            importe = importe - client._TOTAL_PAGADO

            foo = foo & "0,"
            foo = foo & "(select current_timestamp from rdb$database),"
            foo = foo & client._TOTAL_PAGADO & ","

        Else

            foo = foo & "null,"
            foo = foo & "null,"
            foo = foo & "null,"

        End If

        foo = foo & importe

        foo = foo & ")"

        Return foo


    End Function

    Public Sub addClaves(ByRef clients As List(Of ClienteVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
      
        Dim fbCommandClave As FbCommand = Nothing        

        Me.deleteClavesDuplicadas(clients)

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction("NewClavesTx")


            fbCommandClave = sqlConnection.CreateCommand()
            fbCommandClave.Transaction = sqlTransaction

            For Each client As ClienteVO In clients


                If client._MUST_INSERT_CLAVE = True Then
                    fbCommandClave.CommandText = getInsertClientClave(client)
                    fbCommandClave.ExecuteNonQuery()
                End If

      
            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

            Throw ex

        End Try

    End Sub

    Public Sub addNewClients(ByRef clients As List(Of ClienteVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbCommandCargo As FbCommand = Nothing
        Dim fbCommandPrimerAbono As FbCommand = Nothing

        Dim abono As AbonoVO = Nothing

        Me.deleteClientesDuplicados(clients)

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction("NewClientsTx")

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            fbCommandCargo = sqlConnection.CreateCommand()
            fbCommandCargo.Transaction = sqlTransaction

            fbCommandPrimerAbono = sqlConnection.CreateCommand()
            fbCommandPrimerAbono.Transaction = sqlTransaction

            For Each client As ClienteVO In clients

                If client._MUST_INSERT = True Then

                    fbCommand.CommandText = getInsertClientString(client)
                    fbCommand.ExecuteNonQuery()

                    fbCommandCargo.CommandText = getInsertCargoString(client._CARGO)
                    fbCommandCargo.ExecuteNonQuery()

                    If client._TOTAL_PAGADO <> 0 Then
                        ''insertar el total pagado
                        abono = New AbonoVO()
                        abono._IMPORTE = client._TOTAL_PAGADO
                        abono._IMPMON_EXT = client._TOTAL_PAGADO
                        abono._REFER = client._CARGO._REFER
                        abono._NO_FACTURA = client._CARGO._NO_FACTURA
                        abono._DOCTO = 0
                        abono._FECHA_APLI = client._CARGO._FECHA_VENC
                        abono._FECHA_VENC = client._CARGO._FECHA_VENC
                        abono._CVE_CLIE = client._CLAVE

                        fbCommandPrimerAbono.CommandText = Me.getInsertAbonoString(abono)
                        fbCommandPrimerAbono.ExecuteNonQuery()

                    End If

                End If

            Next

            sqlTransaction.Commit()

            addClaves(clients)

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

            Throw ex

        End Try

    End Sub

    Private Sub deleteAbonosDuplicados(ByRef abonos As List(Of AbonoVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlDataReader As FbDataReader = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim count As Integer = 0

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            For Each abono As AbonoVO In abonos

                fbCommand = sqlConnection.CreateCommand()
                fbCommand.CommandText = getSelectAbonoString(abono)

                sqlDataReader = fbCommand.ExecuteReader()

                While sqlDataReader.Read()

                    count = Convert.ToInt32(sqlDataReader(0))

                    If count = 0 Then
                        abono._MUST_INSERT = True
                    Else
                        abono._MUST_INSERT = False
                    End If

                End While

                sqlDataReader.Close()

            Next

        Catch e As FbException

            Throw e

        Finally

            If Not sqlDataReader Is Nothing Then
                sqlDataReader.Close()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Private Sub deleteClavesDuplicadas(ByRef clientes As List(Of ClienteVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlDataReader As FbDataReader = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim count As Integer = 0

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            For Each cliente As ClienteVO In clientes

                fbCommand = sqlConnection.CreateCommand()
                fbCommand.CommandText = Me.getClaveExistenteString(cliente)

                sqlDataReader = fbCommand.ExecuteReader()

                While sqlDataReader.Read()

                    count = Convert.ToInt32(sqlDataReader(0))

                    If count = 0 Then
                        cliente._MUST_INSERT_CLAVE = True
                    Else
                        cliente._MUST_INSERT_CLAVE = False
                    End If

                End While

                sqlDataReader.Close()

            Next

        Catch e As FbException

            Throw e

        Finally

            If Not sqlDataReader Is Nothing Then
                sqlDataReader.Close()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Private Sub deleteClientesDuplicados(ByRef clientes As List(Of ClienteVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlDataReader As FbDataReader = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim count As Integer = 0

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            For Each cliente As ClienteVO In clientes

                fbCommand = sqlConnection.CreateCommand()
                fbCommand.CommandText = getSelectClientString(cliente)

                sqlDataReader = fbCommand.ExecuteReader()

                While sqlDataReader.Read()

                    count = Convert.ToInt32(sqlDataReader(0))

                    If count = 0 Then
                        cliente._MUST_INSERT = True
                    Else
                        cliente._MUST_INSERT = False
                    End If

                End While

                sqlDataReader.Close()

            Next

        Catch e As FbException

            Throw e

        Finally

            If Not sqlDataReader Is Nothing Then
                sqlDataReader.Close()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Public Sub addAbonos(ByRef abonos As List(Of AbonoVO))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim fbCommandUpdate As FbCommand = Nothing


        Me.deleteAbonosDuplicados(abonos)

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction("NewAbonosTx")

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            fbCommandUpdate = sqlConnection.CreateCommand()
            fbCommandUpdate.Transaction = sqlTransaction

            For Each abono As AbonoVO In abonos

                If abono._MUST_INSERT = True Then

                    fbCommand.CommandText = getInsertAbonoString(abono)
                    fbCommand.ExecuteNonQuery()

                    fbCommandUpdate.CommandText = getUpdateClientForAbono(abono)
                    fbCommandUpdate.ExecuteNonQuery()

                  
                End If

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

            Throw ex


        End Try
    End Sub

End Class
