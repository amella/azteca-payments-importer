﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Azteca to Aspel")> 
<Assembly: AssemblyDescription("Importador de saldos y cartera")> 
<Assembly: AssemblyCompany("Grupo Slash Core S. A. de C. V.")> 
<Assembly: AssemblyProduct("Azteca to Aspel")> 
<Assembly: AssemblyCopyright("Copyright © Grupo Slash Core S. A. de C. V. 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("953e426f-3f21-4e35-8a81-cd1ebce7cc3b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.8")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("")> 