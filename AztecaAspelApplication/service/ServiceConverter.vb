﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports FirebirdSql.Data.FirebirdClient

Public Class ServiceConverter

    Private _strRoute As String

    Private _clienteDAO As ClienteDAO = Nothing

    Public Property ClienteDAO()
        Get
            Return _clienteDAO
        End Get
        Set(ByVal value)
            _clienteDAO = value
        End Set
    End Property

    Public Property Route()
        Get
            Return _strRoute
        End Get
        Set(ByVal value)
            _strRoute = value
        End Set
    End Property

    Public Sub processSaldoCartera()

        Dim appInExcel As Excel.Application = New Excel.Application()
        Dim bookIn As Excel.Workbook = appInExcel.Workbooks.Open(_strRoute)

        appInExcel.Workbooks(1).Activate()

        Dim sheetIn1 As Excel.Worksheet = CType(bookIn.Sheets(1), Excel.Worksheet)
        sheetIn1.Select()

        Dim newClients As List(Of ClienteVO) = New List(Of ClienteVO)
        Dim client As ClienteVO = Nothing

        Try

            Dim row As Integer = 2

            Dim lastRow As Integer = sheetIn1.Range("H2").SpecialCells(XlCellType.xlCellTypeLastCell).Row         

            For i As Integer = row To lastRow

                client = New ClienteVO()

                client._CLAVE = sheetIn1.Range("H" & i).Value
                client._NOMBRE = sheetIn1.Range("X" & i).Value
                client._CALLE = sheetIn1.Range("AB" & i).Value
                client._CODIGO = sheetIn1.Range("AC" & i).Value
                client._MUNICIPIO = sheetIn1.Range("Z" & i).Value
                client._ESTADO = sheetIn1.Range("Y" & i).Value
                client._LIMCRED = Double.Parse(sheetIn1.Range("A" & i).Value)
                client._TOTAL_PAGADO = Double.Parse(sheetIn1.Range("B" & i).Value)
                ''client._SALDO = Double.Parse(sheetIn1.Range("C" & i).Value)
                client._COLONIA = sheetIn1.Range("AA" & i).Value

                client._CLAVE = client._CLAVE.Trim
                client._CLAVE = client._CLAVE.PadLeft(10)
                client._NOMBRE = client._NOMBRE.Trim
                client._CALLE = client._CALLE.Trim
                client._CODIGO = client._CODIGO.Trim
                client._MUNICIPIO = client._MUNICIPIO.Trim
                client._ESTADO = client._ESTADO.Trim
                client._COLONIA = client._COLONIA.Trim

                client._CARGO = New CargoVO(client._CLAVE, client._LIMCRED, Date.Parse(sheetIn1.Range("K" & i).Value))

                newClients.Add(client)

            Next

            _clienteDAO.addNewClients(newClients)

        Catch t As FbException

            Throw t

        Catch e As Exception

            Throw e

        Finally

            bookIn.Close(SaveChanges:=False)

            If Not appInExcel Is Nothing Then
                appInExcel.Quit()
                appInExcel = Nothing
            End If

        End Try

    End Sub

    Public Sub processCobranza()

        Dim appInExcel As Excel.Application = New Excel.Application()
        Dim bookIn As Excel.Workbook = appInExcel.Workbooks.Open(_strRoute)

        appInExcel.Workbooks(1).Activate()

        Dim sheetIn1 As Excel.Worksheet = CType(bookIn.Sheets(1), Excel.Worksheet)
        sheetIn1.Select()

        Dim newClients As List(Of ClienteVO) = New List(Of ClienteVO)
        Dim client As ClienteVO = Nothing

        Try

            Dim row As Integer = 2

            Dim lastRow As Integer = sheetIn1.Range("I4").SpecialCells(XlCellType.xlCellTypeLastCell).Row

            Dim abonos As List(Of AbonoVO) = New List(Of AbonoVO)
            Dim abono As AbonoVO = Nothing

            For i As Integer = row To lastRow

                abono = New AbonoVO()
                Dim intClave As Integer
                Dim clave As String = sheetIn1.Range("I" & i).Value
                clave = clave.Trim()
                clave = clave.Substring(2, 9)

                intClave = Integer.Parse(clave)
                clave = intClave
                clave = clave.PadLeft(10)

                Dim numeroOperacion As String = sheetIn1.Range("F" & i).Value

                Dim importeTxt As String = sheetIn1.Range("J" & i).Value
                importeTxt = Replace(importeTxt, ",", "")
                importeTxt = Replace(importeTxt, ".", "")

                Dim importe As Double = Double.Parse(importeTxt)
                abono._IMPORTE = importe
                abono._IMPMON_EXT = importe
                Dim fechaTxt As String = sheetIn1.Range("A" & i).Value
                fechaTxt = fechaTxt.Trim
                Dim fecha As Date = Date.Parse(fechaTxt)
                abono._FECHA_APLI = fecha
                abono._FECHA_VENC = fecha
                abono._CVE_CLIE = clave
                abono._REFER = clave
                abono._NO_FACTURA = clave
                abono._DOCTO = numeroOperacion

                abonos.Add(abono)

            Next

            _clienteDAO.addAbonos(abonos)

        Catch t As FbException

            Throw t

        Catch e As Exception

            Throw e

        Finally

            bookIn.Close(SaveChanges:=False)

            If Not appInExcel Is Nothing Then
                appInExcel.Quit()
                appInExcel = Nothing
            End If

        End Try

    End Sub

End Class
